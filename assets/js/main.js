/*
* Author: Adnan Ali
* Challenge: Admoriis Todo
* Date: 16.02.2022 09:00 AM
*/


function counter()
{
	/**
	 * Quantity counter 
	 */
	var value = 0
	$(".counter #total").val(value);

	// If plus button has click event
	$('.plus').on("click", function() {
		// Add 1 to current value of quantity input
	    value = parseInt(value + 1);
	    // append new value to input element (qty input)
	    $(".counter #total").val(value);
	    $(".counter #total").attr('value', value);
	    // Remove disabled css classes from elements 
	    $('.minus').removeClass('disabled-btn');
	    $('.cart-btn').removeClass('disabled-btn');
	});

	// If minus button has click event
	$('.minus').on("click", function() {
		// If current value of qty input greater than 0
	    if (value > 0) {
	    	// decremment 1 from current value of input
	        value = parseInt(value - 1);
	        // append new value to input element (qty input)
	        $(".counter #total").val(value);
	        $(".counter #total").attr('value', value);
	    } else {
	    	// Otherwise, value is 0
	        value = 0;
	        // append new value to input element (qty input)
	        $(".counter #total").val(value);
	        $(".counter #total").attr('value', value);
	        // Add css disabled class again to to elements such as minus btn and add to cart btn 
	        $('.minus').addClass('disabled-btn');
	        $('.cart-btn').addClass('disabled-btn');
	    }
	});
}

function imageReplacer()
{
	$('.product-images .thumbnails div').on('click', function()
	{
		let imageName = $(this).children('img').attr('src');
		let newImageName = imageName.replace('-thumbnail', '');


		$('.product-images .thumbnails div').attr('class', '');
		// $(this).children('img').attr('src')
		// $('.product-images .primary img').attr('src', $(this).children('img').attr('src'));
		$('.product-images .primary img').attr('src', newImageName);
		$(this).addClass('current');
	});
}

function imageModal()
{
	let modalContainer = `
			<div class="gallery-container product-images">
				<div class="close">
					<svg width="14" height="15" xmlns="http://www.w3.org/2000/svg">
					    <path d="m11.596.782 2.122 2.122L9.12 7.499l4.597 4.597-2.122 2.122L7 9.62l-4.595 4.597-2.122-2.122L4.878 7.5.282 2.904 2.404.782l4.595 4.596L11.596.782Z" fill="#ff7d1a" fill-rule="evenodd" />
					</svg>

				</div>	
				<div class="primary">
					<div class="prev" id="prev">
						<svg width="12" height="18" xmlns="http://www.w3.org/2000/svg"><path d="M11 1 3 9l8 8" stroke="#1D2026" stroke-width="3" fill="none" fill-rule="evenodd" /></svg>
					</div>
					<img src="assets/img/image-product-1.jpg">
					<div class="next" id="next">
						<svg width="13" height="18" xmlns="http://www.w3.org/2000/svg"><path d="m2 1 8 8-8 8" stroke="#1D2026" stroke-width="3" fill="none" fill-rule="evenodd" /></svg>
					</div>
				</div>
				<div class="thumbnails">
					<div class="current"><img src="assets/img/image-product-1-thumbnail.jpg"></div>
					<div><img src="assets/img/image-product-2-thumbnail.jpg"></div>
					<div><img src="assets/img/image-product-3-thumbnail.jpg"></div>
					<div><img src="assets/img/image-product-4-thumbnail.jpg"></div>
				</div>
			</div>
	`;
	let image = $('.product-images .primary img');
	let gallery = $('#gallery');


	image.on('click', function(){
		gallery.append(modalContainer);
		gallery.css('display', 'flex');
	});

	$(document).on('click', '.gallery .close', function(){
		$('.gallery').css('display', 'none');
		$('.gallery').empty();
	});

	$(document).on('click', '.gallery #next', function(){
		let nextImage = $('.gallery .thumbnails .current').next().children('img').attr('src');
		let newImageName = nextImage.replace('-thumbnail', '');

		$('.gallery-container .primary img').attr('src', newImageName);

		$('.gallery .thumbnails div').each(function(){
			$(this).removeClass('current');
			if ($(this).children('img').attr('src') == nextImage) 
			{
				$(this).addClass('current');
				return;
			}
		})

	});

	$(document).on('click', '.gallery #prev', function(){
		let nextImage = $('.gallery .thumbnails .current').prev().children('img').attr('src');
		let newImageName = nextImage.replace('-thumbnail', '');

		$('.gallery-container .primary img').attr('src', newImageName);

		$('.gallery .thumbnails div').each(function(){
			$(this).removeClass('current');
			if ($(this).children('img').attr('src') == nextImage) 
			{
				$(this).addClass('current');
				return;
			}
		})
	});

	$(document).on('click', '.gallery .thumbnails div', function(){
		$('.gallery .thumbnails div').each(function(){
			$(this).removeClass('current');
		});
		$(this).addClass('current');

		let newImageName = $(this).children('img').attr('src').replace('-thumbnail', '');
		$('.gallery-container .primary img').attr('src', newImageName)
	})

	
}

var qtyVal = 0;

function addToCart()
{
	
	$('.cart-btn').on('click', function(){
		let qty = $('.counter #total').val();
		// let qtyVal = 0;
		if (parseInt(qty) <= 0) 
		{
			alert('oh, wait, qty is 0 or less!')
		}
		else
		{
			if ($('.cart-header .icon .total-items')[0]) 
			{
				$('.cart-header .icon .total-items').text(parseInt($('.cart-header .icon .total-items').text()) + parseInt(qty));
				qtyVal = parseInt($('.cart-header .icon .total-items').text()) + parseInt(qty); 
			}
			else
			{
				$('.cart-header .icon').append(`<div class="total-items text-white bg-primary">${parseInt(qty)}</div>`);
				qtyVal = parseInt(qty); 
			}
			
		}
		
		
	})
}

function mobileSlider()
{
	let next = $('.product-images .primary .next');
	let prev = $('.product-images .primary .prev');

	next.on('click', function(){
		alert('next');
	});

	prev.on('click', function(){
		alert('next');
	});
}

function openCart()
{

	let cartIcon = $('.cart-header .icon');
	let cartContainer = $('.cart-header .items');
	let cartContent = $('.cart-header .items .list');
	let clicked = false;

	cartIcon.on('click', function(){


		cartContainer.css('display', 'block');
		cartContent.empty();


		if (qtyVal > 0) 
		{
			cartContent.append(`
									<div class="item">
										<div class="flex align-items-center">
											<img src="assets/img/image-product-1.jpg">
											<div class="details">
												<p>Fall Limited Edition sneakers</p>
												<p>
													$125.00 x 
													<span class="qty">${qtyVal}</span> 
													<span class="price">${Math.round(qtyVal * 125).toFixed(2)}$</span> 
												</p>
											</div>
										</div>
										<div class="action-remove">
											<svg width="14" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
											    <defs>
											        <path
											            d="M0 2.625V1.75C0 1.334.334 1 .75 1h3.5l.294-.584A.741.741 0 0 1 5.213 0h3.571a.75.75 0 0 1 .672.416L9.75 1h3.5c.416 0 .75.334.75.75v.875a.376.376 0 0 1-.375.375H.375A.376.376 0 0 1 0 2.625Zm13 1.75V14.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 1 14.5V4.375C1 4.169 1.169 4 1.375 4h11.25c.206 0 .375.169.375.375ZM4.5 6.5c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Zm3 0c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Zm3 0c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Z"
											            id="a"
											        />
											    </defs>
											    <use fill="#C3CAD9" fill-rule="nonzero" xlink:href="#a" />
											</svg>
											
										</div>
									</div>
			`);
			$('.checkout-btn').css('display', 'block');
		}
		else
		{
			cartContent.append(`
				<p class="empty">Your cart is empty.</p>
			`);
		}

		$('.action-remove').on('click', function(){
			cartContent.empty();
			cartContent.append(`
				<p class="empty">Your cart is empty.</p>
			`);
			$('.checkout-btn').css('display', 'none');
			$('.total-items').hide();
		});

	});

	
}


$(document).ready(function(){
	imageReplacer();
	imageModal();
	counter();
	addToCart();
	openCart();

	$('.mobile-menu-icon').on('click', function(){
		$('.header .header-menu').css('left', 0);
	});
	$('.close-menu').on('click', function(){
		$('.header .header-menu').css('left', '-100%');
	});

 
});